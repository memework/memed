memed
=======

`MEMEwork Daemon`

Core functions of memed:

 - Communicate with Discord
 - Logging what users do on our containers
 - Handling the `rsudo` command
 - Fucking over users by banning them

## The protocol

connect to `log.suck` (unix domain socket).

### how do send packet

```
[L][OP][data : str]

L = little endian encoding of an unsigned int
  Represents the data length

OP = little endian encoding of an int
  OP table soon

data = the data you're sending
```

### give op codes

```
When connecting, you receive an OP 0 with `hello` as the data,
make sure you receive that before continuing.

the only OP code right now is OP 1, which receives a logline,
parses through it and logs it down to Postgres
```