
class Cog:
    def __init__(self, bot):
        self.bot = bot
        self.loop = bot.loop

    @property
    def db(self):
        return self.bot.db
