import time
import discord

from discord.ext import commands

from .common import Cog


class Basic(Cog):
    @commands.command(aliases=['p'])
    async def ping(self, ctx):
        """Ping.
        """

        t1 = time.monotonic()
        m = await ctx.send('pinging...')
        t2 = time.monotonic()

        rtt = (t2 - t1) * 1000
        gw = self.bot.latency * 1000

        await m.edit(content=f'rtt: `{rtt:.1f}ms`, gw: `{gw:.1f}ms`')

    @commands.command()
    async def uptime(self, ctx):
        """Show uptime"""
        sec = round(time.monotonic() - self.bot.init_time)

        m, s = divmod(sec, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)

        await ctx.send(f'Uptime: **`{d} days, {h} hours, '
                       f'{m} minutes, {s} seconds`**')

    @commands.command()
    async def about(self, ctx):
        """Show stuff."""

        em = discord.Embed(title='Sex Dungeon Mistress')
        em.add_field(name='About', value='SDM is a bot made to manage'
                     'memework vps\' (sexhouse) operations to users')

        appinfo = await self.bot.application_info()
        owner = appinfo.owner
        em.add_field(name='Owner',
                     value=f'{owner.mention}, {owner}, (`{owner.id}`)')

        await ctx.send(embed=em)


def setup(bot):
    bot.add_cog(Basic(bot))
