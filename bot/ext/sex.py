import logging
import os
import base64

import discord
from discord.ext import commands

from .common import Cog


class Sex(Cog):
    @commands.command()
    async def me(self, ctx, username: str = None, *, reason: str = None):
        """Request a sexhouse account.
        
        provide an username you want, it has to at least contain your current username.

        provide at least a sane reason to join, and that you'll actually use our
        resources
        
        Please, *please*, specify your account area in your reason.
        Not complying to this will be a denied request.
         - "lite" if you want our normal services, like GitLab and Email
         - "sexhouse" if you want access to other services, like the Main User CT and Owncloud

        thanks
        """
        chan = self.bot.get_channel(self.bot.config.sexr_chan)
        if not chan:
            return await ctx.send('sexhouse request channel not found')

        if ctx.guild:
            await ctx.send('Read your DMs. This command only works in DMs')
            await ctx.author.send(f'Read the helptext to this command with `{self.bot.command_prefix}help me`')
            return

        if not username:
            return await ctx.send('Please provide an username')

        if not reason:
            return await ctx.send('Please provde a reason')

        em = discord.Embed(title=f'Sexhouse request',
                color=discord.Colour(0xf84a6e))

        em.add_field(name='Who', value=ctx.author.mention)
        em.add_field(name='Username', value=username)
        em.add_field(name='Reason', value=reason)

        em.add_field(name='Generated Password',
                value=base64.b64encode(os.urandom(100))[:16].decode())

        await chan.send(embed=em)
        await ctx.send('done!')


def setup(bot):
    bot.add_cog(Sex(bot))

