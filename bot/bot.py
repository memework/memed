import logging
import time

import discord
from discord.ext import commands

log = logging.getLogger(__name__)

exts = [
    'basic',
    'admin',
    'rsudo',
    'sex'
]


class SexDungeonMistressBot(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_time = time.monotonic()

    async def on_command(self, ctx):
        # thanks dogbot ur a good
        content = ctx.message.content

        author = ctx.message.author
        guild = ctx.guild
        checks = [c.__qualname__.split('.')[0] for c in ctx.command.checks]
        location = '[DM]' if isinstance(ctx.channel, discord.DMChannel) else \
                   f'[Guild {guild.name} {guild.id}]'

        log.info('%s [cmd] %s(%d) "%s" checks=%s', location, author,
                 author.id, content, ','.join(checks) or '(none)')


def schedule_bot(loop, config, db):
    mute = ['discord', 'websockets']

    for l in mute:
        d = logging.getLogger(l)
        d.setLevel(logging.INFO)

    bot = SexDungeonMistressBot(
        command_prefix='sex ',
        description='sexhouse management bot',
        owner_id=getattr(config, 'owner_id', None),
        loop=loop
    )

    bot.db = db
    bot.config = config

    try:
        for ext in exts:
            bot.load_extension(f'bot.ext.{ext}')
            log.info('loaded %s', ext)

        return bot
    except:
        log.exception('failed to load %s', ext)
