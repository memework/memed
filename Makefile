OBFUSCATE="./env/bin/pyminifier --obfuscate --gzip"

all:
	${OBFUSCATE} -o build/memed.py memed.py
