CREATE TABLE logs (
       executed_at timestamp without time zone default now(),
       uid int,
       cmd text NOT NULL,
       cwd text
);
